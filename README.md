# AngularJS PWA boilerplate
It's clean, has only the structures to the initial development of a PWA.

### Getting started
- make a clone of this repository `git clone https://recs182@bitbucket.org/recs182-boilerplates/pwa-angular.git project_name`
- delete .git `rm -rf .git`
- install packages `npm i`
- run gulp in a terminal
- see your project at `dist` folder, edit then in `app` folder