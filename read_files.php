<?php
function getDirContents($dir, &$results = array()){
    $files = scandir($dir);

    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            $results[] = str_replace('C:\\Webpages\\save-plataforma\\dist\\', '', $path);
        } else if($value != '.' && $value != '..') {
            getDirContents($path, $results);
            if(!is_dir($path)) {
                $results[] = str_replace('C:\\Webpages\\save-plataforma\\dist\\', '', $path);
            }
        }
    }

    return $results;
}

if( file_exists('dist') ){
    $files = getDirContents('dist/');
    echo json_encode($files);
}