const regexLocal    = /(192\.168\.1|127\.0\.0|localhost)/i;
const url           = location.href;

const PATH_FILES    = url.match(regexLocal) ? '/save-plataforma/dist/' : '';
const CACHE_VERSION = '0.0.1';
const CACHE_FILES   = [
	PATH_FILES + 'css.css',
	PATH_FILES + 'fonts/FontAwesome/FontAwesome.otf',
	PATH_FILES + 'fonts/FontAwesome/fontawesome-webfont.eot',
	PATH_FILES + 'fonts/FontAwesome/fontawesome-webfont.svg',
	PATH_FILES + 'fonts/FontAwesome/fontawesome-webfont.ttf',
	PATH_FILES + 'fonts/FontAwesome/fontawesome-webfont.woff',
	PATH_FILES + 'fonts/FontAwesome/fontawesome-webfont.woff2',
	PATH_FILES + 'fonts/RobotoCondensed/RobotoCondensed-Bold.ttf',
	PATH_FILES + 'fonts/RobotoCondensed/RobotoCondensed-BoldItalic.ttf',
	PATH_FILES + 'fonts/RobotoCondensed/RobotoCondensed-Italic.ttf',
	PATH_FILES + 'fonts/RobotoCondensed/RobotoCondensed-Light.ttf',
	PATH_FILES + 'fonts/RobotoCondensed/RobotoCondensed-LightItalic.ttf',
	PATH_FILES + 'fonts/RobotoCondensed/RobotoCondensed-Regular.ttf',
	PATH_FILES + 'index.html',
	PATH_FILES + 'js.js',
	PATH_FILES + 'libs.js',
	PATH_FILES + 'manifest.json',
	PATH_FILES + 'service-worker.js',
	PATH_FILES + 'views/error/404.html',
	PATH_FILES + 'views/home/home.html',
	PATH_FILES + 'views/main/main.html',
	PATH_FILES + 'views/uib/carousel/carousel.html'
];

//Install stage sets up the cache-array to configure pre-cache content
self.addEventListener('install', event => {
	console.log('[sw] The service worker is being installed.');
	event.waitUntil(precache().then(() => {
		console.log('[sw] Skip waiting on install');
		return self.skipWaiting();
	}));
});


//allow sw to control of current page
self.addEventListener('activate', event => {
	console.log('[sw] Claiming clients for current page');
	return self.clients.claim();
});

self.addEventListener('fetch', event => {
	console.log('[sw] The service worker is serving the asset.'+ event.request.url);
	event.respondWith(fromCache(event.request).catch(fromServer(event.request)));
	event.waitUntil(update(event.request));
});


function precache() {
	return caches.open(CACHE_VERSION).then(cache => {
		return cache.addAll(CACHE_FILES);
	});
}

function fromCache(request) {
	//we pull files from the cache first thing so we can show them fast
	return caches.open(CACHE_VERSION).then(cache => {
		return cache.match(request).then(matching => {
			return matching || Promise.reject('no-match');
		});
	});
}

function update(request) {
	//this is where we call the server to get the newest version of the 
	//file to use the next time we show view
	return caches.open(CACHE_VERSION).then(cache => {
		return fetch(request).then(response => {
			return cache.put(request, response);
		});
	});
}

function fromServer(request){
	//this is the fallback if it is not in the cache to go to the server and get it
	return fetch(request).then(response => response);
}