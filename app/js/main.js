angular.module('app', [
	'app.filters',
	'ui.router',
	'ui.bootstrap',
	'ngSanitize',
	'ngMessages',
    'ui.utils.masks',
	'monospaced.elastic',
])
.run(($rootScope, $document, $window, $state, $transitions) => {
	$rootScope.scroll       = {location: 0, blocked: false};
	$rootScope.current_page = '';
	
	$document.on('scroll', function() {
		$rootScope.$apply(function() {
			$rootScope.scroll.location = $window.scrollY;
		})
	});

	$transitions.onStart({}, () => {
		$rootScope.asideOpen      = false;
		$rootScope.scroll.blocked = false;
	});

	$transitions.onSuccess({}, () => {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
		$rootScope.current_page   = $state.current.name.replace(/[.]/gi, '-');
	});
})
.config(($stateProvider, $urlRouterProvider, $locationProvider)=>{
	isLocalhost(location.href) ? $locationProvider.html5Mode(false) : $locationProvider.html5Mode(true);
 	$urlRouterProvider.otherwise('/');
	
	$stateProvider
	.state('main', {
		templateUrl: 'views/main/main.html',
	})
	.state('main.home', {
		url: '/',
		templateUrl: 'views/home/home.html',
	})
})
//////////////////////////////////////////
// Controllers
//////////////////////////////////////////

//////////////////////////////////////////
// Components
//////////////////////////////////////////

//////////////////////////////////////////
// Directive
//////////////////////////////////////////
.directive('dErrSrc', () => new errSrc())

//////////////////////////////////////////
// Services
//////////////////////////////////////////
.service('Post', Post)
